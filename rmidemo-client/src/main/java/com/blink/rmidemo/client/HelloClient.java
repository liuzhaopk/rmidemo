package com.blink.rmidemo.client;

import com.blink.rmidemo.service.IHello;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * <Description> 客户端测试，在客户端调用远程对象上的远程方法，并返回结果。</Description>
 * <ClassName> HelloClient</ClassName>
 *
 * @Author liuxianzhao
 * @Date 2018年01月25日 13:25
 */
public class HelloClient {
    public static void main(String args[]) {
        try {
            //在RMI服务注册表中查找名称为RHello的对象，并调用其上的方法 
            IHello rhello = (IHello) Naming.lookup("rmi://localhost:8888/RHello");
            System.out.println(rhello.helloWorld());
            System.out.println(rhello.sayHelloToSomeBody("熔岩"));
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}