package com.blink.rmidemo.service;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * <Description> 定义一个远程接口，必须继承Remote接口，
 * 其中需要远程调用的方法必须抛出RemoteException异常 </Description>
 * <ClassName> IHello</ClassName>
 *
 * @Author liuxianzhao
 * @Date 2018年01月25日 13:22
 */
public interface IHello extends Remote {

    /**
     * 简单的返回“Hello World！"字样
     *
     * @return 返回“Hello World！"字样
     * @throws RemoteException
     */
    public String helloWorld() throws RemoteException;

    /**
     * 一个简单的业务方法，根据传入的人名返回相应的问候语
     *
     * @param someBodyName 人名
     * @return 返回相应的问候语
     * @throws RemoteException
     */
    public String sayHelloToSomeBody(String someBodyName) throws RemoteException;
}